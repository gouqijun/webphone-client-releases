#!/bin/bash
current_directory="`pwd`"
release="`ls $current_directory |grep release-|cut -c9-`"
OML_PATH="/opt/omnileads/ominicontacto"

echo "** [webphone-client] Checking OmniLeads version"
oml_version=`cat ${OML_PATH}/ominicontacto_app/version.py |grep OML_BRANCH |awk -F "=" '{gsub(/"/, "", $2); print $2}'`
if [[ ${oml_version} == *"release-1.2"* ]] || [[ ${oml_version} == *"release-1.1"* ]]; then
  echo "** OMniLeads version not supported to install this addon, please first upgrade your instance"
  exit 1
fi
echo "** [webphone-client] Copying the webphone_client_app folder to ${OML_PATH}"
cp -a release-${release}/webphone_client_app ${OML_PATH}
content="`cat $OML_PATH/ominicontacto/settings/addons.py`"
if [[ $content != *"webphone_client_app"* ]]; then
  echo "** [webphone-client] Editing settings file addons.py"
  echo "ADDONS_APPS += ['webphone_client_app',]" >> ${OML_PATH}/ominicontacto/settings/addons.py
  echo "ADDON_URLPATTERNS +=[ (r'^', 'webphone_client_app.urls'), ]" >> ${OML_PATH}/ominicontacto/settings/addons.py
  echo "** [webphone-client] Adding webphone client constance variables"
  sed -i "/^.*WEBPHONE_CLIENT_ENABLED.*/a \ \ \ \ 'WEBPHONE_CLIENT_TTL': (1200, 'WEBPHONE_CLIENT_TTL', int)," ${OML_PATH}/ominicontacto/settings/addons.py
fi
