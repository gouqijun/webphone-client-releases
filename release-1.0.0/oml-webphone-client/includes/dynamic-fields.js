jQuery(document).ready(function( $ ){
  jQuery(document).off().on('click',' .addrowbtn', function(event) {
       event.preventDefault();
    var row = jQuery(this).closest(".tableclass").find("tr:last").clone();

    row.removeClass( 'tablerowclone' );
    //row.insertBefore( '#repeatable-fieldset-one tbody>tr:last' );
    row.insertBefore(jQuery(this).closest(".tableclass").find("tbody tr.tablerowclone"));
    return false;
  });

  jQuery(document).on('click', '.remove-row' , function() {
    var RowLenth  = jQuery(this).closest("tbody").children().length;

        if(RowLenth>2){
            jQuery(this).parents('tr').remove();
        }
    return false;
  });
});
