<h2><?php esc_attr_e( 'OML WebphoneClient', 'WpAdminStyle' ); ?></h2>

<p><strong>In this page, set the configuration to make work your webphoneclient. More information about what to add in the fields, see the repository of the addon  <code><a href="https://gitlab.com/omnileads/webphone-client-releases.git" target="_blank">WebphoneClient Module</a></code></strong></p>
<form class="webphoneclient_form" action="" method="post">
<table class="form-table">
	<tr>
		<th class="row-title" colspan="2"><?php esc_attr_e( 'WebphoneClient user credentials', 'WpAdminStyle' ); ?></th>
	</tr>
	<tr valign="top">
		<td scope="col" ><strong><?php esc_attr_e('Username:', 'WpAdminStyle'); ?></strong></td>
		<h2 style="color:#FF0000"><strong><?php echo $msg; ?></strong></h2>
		<td scope="col"><strong><?php esc_attr_e( 'Password:', 'WpAdminStyle' ); ?></strong></td>
	</tr>
  <tr valign="top">
		<input type="hidden" name="webphoneclient_form_submitted" value="Y">
    <td scope="col" ><input name="webphoneclient_user" type="text" value="<?php echo $webphoneclient_user ?>" placeholder="Enter your username" class="regular-text" /><br></td>
    <td scope="col"><input name="webphoneclient_pass" type="password" value="" placeholder="Enter your password" class="regular-text" /></td>
  </tr>
	<th class="row-title" colspan="2"><?php esc_attr_e( 'OMniLeads information', 'WpAdminStyle' ); ?></th>
</tr>
<tr valign="top">
  <td scope="col"><strong><?php esc_attr_e( 'OMniLeads hostname: (The DNS you use to conect to your OMniLeads via web)', 'WpAdminStyle' ); ?></strong></td>
	<td scope="col"><strong><?php esc_attr_e( 'OMniLeads port: (The port you use to conect to your OMniLeads via web)', 'WpAdminStyle' ); ?></strong></td>
</tr>
<tr valign="top">
  <td scope="col"><input name="omnileads_hostname" type="text" value="<?php echo $omnileads_hostname ?>" placeholder="Enter your hostname" class="regular-text" /></td>
  <td scope="col" ><input name="omnileads_port" type="text" value="<?php echo $omnileads_port ?>" placeholder="443, for example" class="regular-text" /><br></td>
</tr>
</table>
<input class="button-primary" type="submit" name="webphoneclient_submit" value="<?php esc_attr_e( 'Save' ); ?>" />
</form>
