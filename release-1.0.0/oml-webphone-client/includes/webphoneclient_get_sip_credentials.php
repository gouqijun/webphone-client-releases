<?php

function webphoneclient_get_sip_credentials( $webphoneclient_user, $webphoneclient_pass, $omnileads_hostname, $omnileads_port, $where_from ) {
	$user = urlencode( $webphoneclient_user );
	$secret = urlencode( $webphoneclient_pass );
	$concatenated = $user . ':' . $secret;
	$encoded = base64_encode( $concatenated );
	$args = array(
		'headers' => array(
			'Authorization' => 'Basic ' . $encoded,
			'Content-Type' => 'application/json',
		//	'sslverify' => false
		),
	);

	$api_url = 'https://' . $omnileads_hostname . ':' . $omnileads_port . '/api/v1/webphone/credentials/';
	$response = wp_remote_post( $api_url, $args );
	if ( is_wp_error( $response ) ) {
		$error_message = $response->get_error_message();
		echo '<pre style="color:#FF0000"><strong> API Connection test result: ';
		print_r($error_message);
		echo '</strong></pre>';
		error_log("Something went wrong: $error_message");
	} else {
		$body = wp_remote_retrieve_body( $response );
		$body = json_decode( $body, true );
		if ( isset( $body[status] ) ) {
			if ( $where_from == "admin" ) {
				echo '<pre style="color:#008000"><strong> OML API Connection test result: ';
				print_r("Connection was successful");
				echo '</strong></pre>';
			}
			$out['sip_user'] = $body[sip_user];
			$out['sip_password'] = $body[sip_password];
		} else {
		  	echo '<pre style="color:#FF0000"><strong> API Connection test result: ';
				print_r($body[detail]);
				echo '</strong></pre>';
				//$out = "";
		}
	}
	return $out;
}

?>
