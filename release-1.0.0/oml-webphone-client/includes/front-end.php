<?php

echo $before_widget;

$phone_green = plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/phone_green.png';
$powered_by = plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/powered_by.png';
$phone_red = plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/phone_red.png';
$numpad = plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/numpad.png';

if ( $phone_number_1 != '' && $button_name_1 != '') {
  $line =  "{'phone': '" . $phone_number_1 . "', 'name': '" . $button_name_1 . "', 'index': '0'},";
}
if ( $phone_number_2 != '' && $button_name_2 != '') {
  $line .=  "{'phone': '" . $phone_number_2 . "', 'name': '" . $button_name_2 . "', 'index': '1'},";
}
if ( $phone_number_3 != '' && $button_name_3 != '') {
  $line .=  "{'phone': '" . $phone_number_3 . "', 'name': '" . $button_name_3 . "', 'index': '2'},";
}
if ( $phone_number_4 != '' && $button_name_4 != '') {
  $line .=  "{'phone': '" . $phone_number_4 . "', 'name': '" . $button_name_4 . "', 'index': '3'},";
}

//echo $before_title . $title . $after_title;
echo "
<div id=\"webphone-container\" class=\"" . $container_class . "\"></div>
<script type=\"text/javascript\">
 var phone_controller = undefined;
 var miniPhoneConfig = {
   'sip_user': '" . $sip_credentials['sip_user'] . "',
   'sip_password': '" . $sip_credentials['sip_password'] . "',
   'KamailioHost': '" . $kamailio_host . "',
   'WebSocketHost': '" . $omnileads_hostname . "',
   'WebSocketPort': '" . $omnileads_port . "',
   'webphone_title': '" . $title . "',
   'container_id': 'webphone-container',
   'container_class': '" . $container_class . "',
   'webphone_keys_id': '" . $webphone_keys_id . "',
   'phone_key_pad_id': '" . $phone_key_pad_id . "',
   'phone_actions_id': '" . $phone_actions_id . "',
   'buttons_class': 'key_pad_button',
   'non_phone_actions_id': '" . $non_phone_actions_id . "',
   'show_keypad_button_id': 'show_keypad_button_id',
   'footer_img': '" . $footer_img . "',
   'phone_green': '" . $phone_green . "',
   'phone_red': '" . $phone_red . "',
   'powered_by': '" . $powered_by . "',
   'numpad': '" . $numpad . "',
   'destinations': [
     ". $line . "
   ],
   'key_pad_buttons' : [
     {
       'div': [
         {'button_label': '1', 'button_value': '1'},
         {'button_label': '2', 'button_value': '2'},
         {'button_label': '3', 'button_value': '3'},
       ],
     },
     {
       'div': [
         {'button_label': '4', 'button_value': '4'},
         {'button_label': '5', 'button_value': '5'},
         {'button_label': '6', 'button_value': '6'},
       ],
     },
     {
       'div': [
         {'button_label': '7', 'button_value': '7'},
         {'button_label': '8', 'button_value': '8'},
         {'button_label': '9', 'button_value': '9'},
       ],
     },
     {
       'div': [
         {'button_label': 'asterisk', 'button_value': '*'},
         {'button_label': '0', 'button_value': '0'},
         {'button_label': 'hashtag', 'button_value': '#'},
       ],
     },
   ],
   'extra_headers': [
     'OML-client: 110022993388',
     'X-Bar: bar',
   ],
 };
 jQuery(document).ready(function() {
    phone_controller = new PhoneJSController(miniPhoneConfig);
  });

</script>
";
echo $after_widget;

?>
