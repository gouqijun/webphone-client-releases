<p>
  <label>Title</label>
  <input class="widefat" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
  <label>Kamailio Host</label>
  <input class="widefat" name="<?php echo $this->get_field_name('kamailio_host'); ?>" type="text" value="<?php echo $kamailio_host; ?>" />
  <p><strong> Styling options </strong></p>
  <label>container_class</label>
  <input class="widefat" name="<?php echo $this->get_field_name('container_class'); ?>" type="text" value="<?php echo $container_class; ?>" />
  <label>webphone_keys_id</label>
  <input class="widefat" name="<?php echo $this->get_field_name('webphone_keys_id'); ?>" type="text" value="<?php echo $webphone_keys_id; ?>" />
  <label>phone_key_pad_id</label>
  <input class="widefat" name="<?php echo $this->get_field_name('phone_key_pad_id'); ?>" type="text" value="<?php echo $phone_key_pad_id; ?>" />
  <label>phone_actions_id</label>
  <input class="widefat" name="<?php echo $this->get_field_name('phone_actions_id'); ?>" type="text" value="<?php echo $phone_actions_id; ?>" /> </ br>
  <label>non_phone_actions_id</label>
  <input class="widefat" name="<?php echo $this->get_field_name('non_phone_actions_id'); ?>" type="text" value="<?php echo $non_phone_actions_id; ?>" />
  <label>footer_img</label>
  <input class="widefat" name="<?php echo $this->get_field_name('footer_img'); ?>" type="text" value="<?php echo $footer_img; ?>" />
  <table class="form-table">
  	<tr>
      <p>Call Buttons</p>
  	</tr>
  	<tr valign="top">
  		<td scope="col">Phone Number</td>
  		<td scope="col">Button Name</td>
  	</tr>
  </table>
      <ul>
        <li><input size="15" name="<?php echo $this->get_field_name('phone_number_1'); ?>" type="text" value="<?php echo $phone_number_1; ?>"/>
        <input size="15" name="<?php echo $this->get_field_name('button_name_1'); ?>" type="text" value="<?php echo $button_name_1; ?>" /><br /></li>
        <li><input size="15" name="<?php echo $this->get_field_name('phone_number_2'); ?>" type="text" value="<?php echo $phone_number_2; ?>"/>
        <input size="15" name="<?php echo $this->get_field_name('button_name_2'); ?>" type="text" value="<?php echo $button_name_2; ?>" /><br /></li>
        <li><input size="15" name="<?php echo $this->get_field_name('phone_number_3'); ?>" type="text" value="<?php echo $phone_number_3; ?>"/>
        <input size="15" name="<?php echo $this->get_field_name('button_name_3'); ?>" type="text" value="<?php echo $button_name_3; ?>" /><br /></li>
        <li><input size="15" name="<?php echo $this->get_field_name('phone_number_4'); ?>" type="text" value="<?php echo $phone_number_4; ?>"/>
        <input size="15" name="<?php echo $this->get_field_name('button_name_4'); ?>" type="text" value="<?php echo $button_name_4; ?>" /><br /></li>
      </ul>
</p>
