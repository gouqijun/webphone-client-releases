<?php
require plugin_dir_path( __FILE__ ) . 'includes/webphoneclient_get_sip_credentials.php';

/**
 * The plugin bootstrap file
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Plugin_Name
 *
 * @wordpress-plugin
 * Plugin Name:       OMniLeads WebphoneClient
 * Plugin URI:        http://example.com/plugin-name-uri/
 * Description:       WordPress plugin to add the OMniLeads webphoneclient to your wordpress page, more information about this OMniLeads addon, go to https://omnileads.net/addons
 * Version:           1.0.0
 * Author:            Freetech Solutions
 * Author URI:        https://www.freetechsolutions.com.ar/
 * License:           GPL-3.0+
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.txt
 * Text Domain:       plugin-name
 * Domain Path:       /languages
 */


// Esta función tiene los nombres que va a usar wordpress para identificar este plugin en su base de plugins, se usa la acción admin_menu para añadir el menu correspondiente al "Ajustes"
function webphoneclient_menu(){
	add_options_page(
		'OMniLeads WebphoneClient plugin',
		'OMniLeads WebphoneClient',
		'manage_options',
		'oml_webphoneclient',
		'webphoneclient_options_page'
	);
}
add_action( 'admin_menu','webphoneclient_menu' );

//Esta función añade la lógica para cuando el admin ingresa datos de la página de ajustes del plugin.
function webphoneclient_options_page() {
	$msg = '';
	if ( !current_user_can( 'manage_options' ) ) {
		wp_die( 'You dont have sufficient permissions to access this page' );
	}
	/* Check that our form has been submitted  */
	if ( isset( $_POST['webphoneclient_form_submitted'] ) ) {
		$hidden_field = esc_html( $_POST['webphoneclient_form_submitted'] );
   	if ( $hidden_field == 'Y' ) {
			$webphoneclient_user = esc_html( $_POST['webphoneclient_user'] );
			$webphoneclient_pass = esc_html( $_POST['webphoneclient_pass'] );
			$omnileads_hostname = esc_html( $_POST['omnileads_hostname'] );
			$omnileads_port = esc_html( $_POST['omnileads_port'] );

			if ( !isset($webphoneclient_user) || $webphoneclient_user == '' || !isset($webphoneclient_pass) || $webphoneclient_pass == '' || !isset($omnileads_hostname) || $omnileads_hostname == '' || !isset($omnileads_port) || $omnileads_port == '')
				$msg = 'One or more fields are empty, please fill all the fields';
			else {
				$options['webphoneclient_user'] = $webphoneclient_user;
				$options['webphoneclient_pass'] = $webphoneclient_pass;
				$options['omnileads_hostname'] = $omnileads_hostname;
				$options['omnileads_port'] = $omnileads_port;
				//$msg = 'Data was sucessfully saved';
				webphoneclient_get_sip_credentials( $webphoneclient_user,$webphoneclient_pass,$omnileads_hostname,$omnileads_port,"admin" );
				update_option( 'oml_webphoneclient', $options );
			}
   	}
 	}
	$options = get_option( 'oml_webphoneclient' );
	if ($options != '') {
		$webphoneclient_user = $options['webphoneclient_user'];
		$webphoneclient_pass = $options['webphoneclient_pass'];
		$omnileads_hostname = $options['omnileads_hostname'];
		$omnileads_port = $options['omnileads_port'];
	}

	require( 'includes/options-page-wrapper.php' );

}

//Esta clase contiene la lógica de creación del widget. Se puede decir que el widget es el elemento que añade el javascript, html y css que va a tener el webphoneclient.
class WebphoneClient_Widget extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct( false, 'OML WebphoneClient Widget' );

	}

	function widget( $args, $instance ) {
		extract( $args );
		$title = $instance['title'];
		$kamailio_host = $instance['kamailio_host'];
		$container_class = $instance['container_class'];
		$webphone_keys_id = $instance['webphone_keys_id'];
		$phone_actions_id = $instance['phone_actions_id'];
		$phone_key_pad_id = $instance['phone_key_pad_id'];
		$non_phone_actions_id = $instance['non_phone_actions_id'];
		$footer_img = $instance['footer_img'];

		$phone_number_1 = $instance['phone_number_1'];
		$phone_number_2 = $instance['phone_number_2'];
		$phone_number_3 = $instance['phone_number_3'];
		$phone_number_4 = $instance['phone_number_4'];

		$button_name_1 = $instance['button_name_1'];
		$button_name_2 = $instance['button_name_2'];
		$button_name_3 = $instance['button_name_3'];
		$button_name_4 = $instance['button_name_4'];

		$options = get_option( 'oml_webphoneclient' );
		$webphoneclient_user = $options['webphoneclient_user'];
		$webphoneclient_pass = $options['webphoneclient_pass'];
		$omnileads_hostname = $options['omnileads_hostname'];
		$omnileads_port = $options['omnileads_port'];
		$sip_credentials = webphoneclient_get_sip_credentials($webphoneclient_user,$webphoneclient_pass,$omnileads_hostname,$omnileads_port,"widget");
		require( 'includes/front-end.php' );
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['kamailio_host'] = strip_tags( $new_instance['kamailio_host'] );
		$instance['container_class'] = strip_tags( $new_instance['container_class'] );
		$instance['webphone_keys_id'] = strip_tags( $new_instance['webphone_keys_id'] );
		$instance['phone_actions_id'] = strip_tags( $new_instance['phone_actions_id'] );
		$instance['phone_key_pad_id'] = strip_tags( $new_instance['phone_key_pad_id'] );
		$instance['non_phone_actions_id'] = strip_tags( $new_instance['non_phone_actions_id'] );
		$instance['footer_img'] = strip_tags( $new_instance['footer_img'] );

		$instance['phone_number_1'] = strip_tags( $new_instance['phone_number_1'] );
		$instance['phone_number_2'] = strip_tags( $new_instance['phone_number_2'] );
		$instance['phone_number_3'] = strip_tags( $new_instance['phone_number_3'] );
		$instance['phone_number_4'] = strip_tags( $new_instance['phone_number_4'] );

		$instance['button_name_1'] = strip_tags( $new_instance['button_name_1'] );
		$instance['button_name_2'] = strip_tags( $new_instance['button_name_2'] );
		$instance['button_name_3'] = strip_tags( $new_instance['button_name_3'] );
		$instance['button_name_4'] = strip_tags( $new_instance['button_name_4'] );

		return $instance;
	}

	function form( $instance ) {
		$title = esc_attr( $instance['title'] );
		$kamailio_host = esc_attr( $instance['kamailio_host'] );
		$container_class = esc_attr( $instance['container_class'] );
		$webphone_keys_id = esc_attr( $instance['webphone_keys_id'] );
		$phone_actions_id = esc_attr( $instance['phone_actions_id'] );
		$phone_key_pad_id = esc_attr( $instance['phone_key_pad_id'] );
		$non_phone_actions_id = esc_attr( $instance['non_phone_actions_id'] );
		$footer_img = esc_attr( $instance['footer_img'] );

		$phone_number_1 = esc_attr( $instance['phone_number_1']);
		$phone_number_2 = esc_attr( $instance['phone_number_2']);
		$phone_number_3 = esc_attr( $instance['phone_number_3']);
		$phone_number_4 = esc_attr( $instance['phone_number_4']);

		$button_name_1 = esc_attr( $instance['button_name_1']);
		$button_name_2 = esc_attr( $instance['button_name_2']);
		$button_name_3 = esc_attr( $instance['button_name_3']);
		$button_name_4 = esc_attr( $instance['button_name_4']);


		require( 'includes/widget-fields.php' );
	}
}

function webphoneclient_register_widgets() {
	register_widget( 'WebphoneClient_Widget' );
}
add_action( 'widgets_init', 'webphoneclient_register_widgets' );

// include custom jQuery
function shapeSpace_include_custom_jquery() {
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', plugins_url('oml-webphone-client/public/js/jquery-3.3.1.slim.min.js'), array(), null);
}
//add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');

function webphoneclient_frontend_scripts() {

//	wp_enqueue_script( 'webphoneclient_jquery' , plugins_url('oml-webphone-client/public/js/jquery-3.3.1.slim.min.js'), null );
	wp_enqueue_script( 'webphoneclient_state_machine' , plugins_url('oml-webphone-client/public/js/state-machine-min.js'), array('jquery'), null );
	wp_enqueue_script( 'webphoneclient_JsSIP' , plugins_url('oml-webphone-client/public/js/jssip.js'), array('jquery'), null );
	wp_enqueue_script( 'webphoneclient_mustache' , plugins_url('oml-webphone-client/public/js/mustache-3.0.2.min.js'), array('jquery'), null );
	wp_enqueue_script( 'webphoneclient_phoneJsTemplate' , plugins_url('oml-webphone-client/public/js/phoneJsTemplate.js'), array('jquery'), null );
	wp_enqueue_script( 'webphoneclient_phoneJsFSM' , plugins_url('oml-webphone-client/public/js/phoneJsFSM.js'), array('jquery'), null );
	wp_enqueue_script( 'webphoneclient_phoneJsView' , plugins_url('oml-webphone-client/public/js/phoneJsView.js'), array('jquery'), null );
	wp_enqueue_script( 'webphoneclient_miniPhoneJs' , plugins_url('oml-webphone-client/public/js/miniPhoneJs.js'), array('jquery'), null );
	wp_enqueue_script( 'webphoneclient_phoneJsController' , plugins_url('oml-webphone-client/public/js/phoneJsController.js'), array('jquery'), null );

}
add_action( 'wp_enqueue_scripts', 'webphoneclient_frontend_scripts' );

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function activate_plugin_name() {
	//require_once plugin_dir_path( __FILE__ ) . 'includes/class-plugin-name-activator.php';
	//Plugin_Name_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function webphoneclient_deactivate() {
	//require_once plugin_dir_path( __FILE__ ) . 'includes/class-plugin-name-deactivator.php';
	//Plugin_Name_Deactivator::deactivate();
	$option_name = 'oml_webphoneclient';
	delete_option($option_name);
	delete_site_option($option_name);

}

//register_activation_hook( __FILE__, 'activate_plugin_name' );
register_deactivation_hook( __FILE__, 'webphoneclient_deactivate' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/webphoneclient-name.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_name() {

	$plugin = new Plugin_Name();
	$plugin->run();

}
run_plugin_name();
