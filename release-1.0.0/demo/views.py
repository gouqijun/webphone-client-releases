# -*- coding: utf-8 -*-
from flask import Flask, render_template
import requests
from requests.auth import HTTPBasicAuth


app = Flask(__name__)

config = {
    'URL_API_CREDENTIALS': 'https:/OML_URL/api/v1/webphone/credentials/',
    'client_username': 'wepbhone_user',
    'client_password': 'webphone_pass',
}


def get_credentials(username, password):
    res = requests.post(config['URL_API_CREDENTIALS'],
                        auth=HTTPBasicAuth(username, password),
                        verify=False)
    if res.status_code == 200:
        response = res.json()
        if response['status'] == 'OK':
            return (response['sip_user'], response['sip_password'])

    return (None, None)


@app.route('/')
def index():
    sip_user, sip_password = get_credentials(config['client_username'],
                                             config['client_password'])
    return render_template(
        'index.html',
        sip_user=sip_user,
        sip_password=sip_password
    )


if __name__ == "__main__":
    app.run(ssl_context='adhoc')
