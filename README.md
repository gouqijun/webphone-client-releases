
# Webphone Client Module

[<p align="center"><img src="doc_images/wp_webphone.png" /></p>](https://www.freetechsolutions.com.ar/contacto/)

In this repository you will find the files required to install the [OMniLeads](https://gitlab.com/omnileads/ominicontacto) addon **WebPhone Client**.
These is a code that you can embed into your web page to let your customers call your bussiness **just with one click**, without another tool more than **the browser**.

## Requirements.

1. An OMniLeads instance with **release-1.3.2** minimum installed. This OMniLeads **must be** accesible from Internet and must work with **trusted certificates**.

**Note:** If you don't have a trusted certificate we encourage you to get your own using [Let's Encrpyt](https://letsencrypt.org).

2. Your webpage must be served with HTTPS. (WebRTC requirement)

## Leave OMniLeads configuration in our hands!!!

You don't need to have your own OMniLeads instance, we can provide you an username and password to connect to our hosted OMniLeads 24/7. Just let us know [contacting with us](https://www.freetechsolutions.com.ar/contacto/).

**Take a look, there you have an example of the WebphoneClient!!!** <br/>
**If you decide to use our hosted OMniLeads, you can go directly to** [Embedding the webphone to your page section](#embedding-the-webphone-to-your-web-page)

## Steps to install the WebPhone Module.

1. Execute the script install.sh: **./install.sh**
2. Restart django to get new code:
    - **For AIO:** service omnileads reload
3. Log in with your admin account and go to the admin page https://OML_HOST/admin/. Go to constance section and click on "Config"

<p align="center"><img src="doc_images/django_admin.png" /></p>

4. Set *WEBPHONE_CLIENT_ENABLED* to True checking the box. This will enable the module locally.
5. Set *WEBPHONE_CLIENT_TTL*. The default WebPhone Credentials Time to live is 20 minutes. (this variable will be explained in ***Getting WebPhone login credentials via API*** section, see the note there)

<p align="center"><img src="doc_images/constance.png" /></p>

After that, click in save

6. Make sure you have registered your OMniLeads instance on the 'Help --> Register' section.

7. Send a mail to info@omnileads.net. Freetech Solutions will answer youe mail to get your Registration Key assigned to this module after buying it

## Configuring a WebPhone Client.

With WEBPHONE_CLIENT_ENABLED set to True, now you can create a WebPhone Client User using your Admin account.

1. Go to Users and groups -> New User. There you will see that is a new check box "Is for Webphone client". Create the user with this box checked:

<p align="center"><img src="doc_images/client_user.png" /></p>

2. You can see the user created under Users and Groups -> Wephone Clients

<p align="center"><img src="doc_images/webphone_client.png" /></p>

**NOTE:** keep in mind the username and password for this user. They are going to be needed for obtaining your SoftPhone login credentials via the API.

## <a name="embedding-the-webphone-to-your-web-page"></a> Embedding the webphone to your web page

**If your webpage is hosted by Wordpress CMS you can skip next two sections and go directory to** [WordPress Plugin section](#wordpress-plugin)

**Including the webphone JavaScript code to your html file:**

1. Copy all the .js files in the /static/js folder to your web server.

You can take a look to the file [index.html](release-1.0.0/demo/templates/index.html) to see a template of what you need to modify to the page you want to add the WebPhone. Anyway, we are going to describe what to do:

2. Include all the .js files in your html file as the index.html template file. **Is very important to keep the order of the *js files provided below**.
```
{
  <script type="text/javascript" src="js/jquery-3.3.1.slim.min.js"></script>
	<script type="text/javascript" src="js/state-machine-min" ></script>
	<script type="text/javascript" src="js/jssip.js" ></script>
	<script type="text/javascript" src="js/mustache-3.0.2.min.js" ></script>
  <script type="text/javascript" src="js/phoneJsTemplate.js" ></script>
  <script type="text/javascript" src="js/phoneJsFSM.js" ></script>
	<script type="text/javascript" src="js/phoneJsView.js" ></script>
	<script type="text/javascript" src="js/miniPhoneJs.js"></script>
	<script type="text/javascript" src="js/phoneJsController.js"></script>
}
```
3. Include the next <script></script> block, where is the context of the JavaScript module. Down below is the explanation of the variables:

```
{
  <script type="text/javascript">
   var phone_controller = undefined;
   var miniPhoneConfig = {
     'sip_user': "<<sip_user>>",    // The sip User obtained from the POST request to the API
     'sip_password': "<<sip_password>>",    // The sip Password obtained from the POST request to the API
     'KamailioHost': "X.X.X.X",     // The LAN IP of your OMniLeads instance
     'WebSocketPort': "443",    // The Port you use to connect in web URL
     'WebSocketHost': "OML_HOST",    // The DNS you use to conect in web URL
     'container_id': 'webphone-container',  // The ID of the <div> containing all the webphone
     'container_class': 'webphone-body',    // The class of the <div> containing all the webphone
     'webphone_keys_id': 'webphone_keys',   // The ID of the <div> containing all the webphone buttons
     'phone_key_pad_id': 'phone_key_pad',   // The ID of the <div> containing the numpad
     'phone_actions_id': 'phone_actions',   // The ID of the <div> containing the buttons for make the calls
     'non_phone_actions_id': 'non_phone_actions', // The ID of the DIV containing the non calls buttons (show numpad and end call)
     'footer_img': 'footer_img',  // The ID of the DIV containing the footer (FTS logo)
     'phone_green': '/path/to/img', // The path to the phone_green image
     'phone_red': '/path/to/img',  // The path to the phone_red image
     'powered_by': '/path/to/img',  // The path to the powered_by image
     'numpad': '/path/to/img',  // The path to the numpad image
     'buttons_class': 'key_pad_button',   // The class of the <div> containting the buttons of the numpad
     'show_keypad_button_id': 'show_key_pad', // The ID of the <div> containing the button "Toggle Keypad"
     'destinations': [                        // These are the buttons that are going to launch the calls, you must to configure the number to call in 'phone', and the 'name' will be displayed
       {'phone': '01155001121', 'name': 'Sales', 'index': '0'},
       {'phone': '01177660011', 'name': 'Help Desk', 'index': '1'},
       {'phone': '01177660012', 'name': 'Suggestions', 'index': '2'},
     ],
     'extra_headers': [                       // SIP headers you want to add to the INVITE of call
       'OML-client: 110022993388',
       'X-Bar: bar',
     ],
}
```

**Changing the WebPhone's html content:**

Take a look to the [phoneJsTemplate.js](release-1.0.0/demo/static/js/phoneJsTemplate.js) file. This file defines a variable which will render/generate the html content of webphone.
This is possible thanks to a JavaScript library called [mustache](https://mustache.github.io/).

You can change the HTML template as you wish but keep every mustache variable. They are defined like this: *{{{{webphone_keys_id}}}}*

**Getting WebPhone login credentials via API**

The username and password you issued for your WebPhone Client User are used to get the credentials to connect to your OMniLeads telephony system as a valid user of the system.
OmniLeads provides an API for getting those credentials so you can embed them on every SoftPhone.
To get the credentials make a POST request to 'https://OML_HOST/api/v1/webphone/credentials/' using a Basic Authentication Schema.

**Note:** The SIP credentials provided by the API are ephemeral. They become obsolete after the time stablished in the *WEBPHONE_CLIENT_TTL* variable. The credentials are refreshed every time the API is consulted, and it is every time that a customer make a call in webphone. We set the default of this variable as an estimated time of call duration, but if you think that some calls of the bussiness you are serving can long more consider increasing this variable value.

This is an example of a request to the API using Postman, is just to show you what the API should response to your request:

<p align="center"><img src="doc_images/postman_example.png" /></p>

**You can see an example of the code to connect to the API and get credentials in demo folder, file** [views.py](release-1.0.0/demo/views.py)


## SoftPhone Demo

In this repository you will also find a simple Flask demo version what will make all proccesses described, it is in the "demo" folder. This demo was tested in a Ubuntu 18.04.2 LTS.
To test it locally you must:

1. Edit the config object at the 'views.py' file to match your configuration.
```
config = {
     'URL_API_CREDENTIALS': 'https://OML_HOST/api/v1/webphone/credentials/', // Your OML instance
     'client_username': 'client_username',   // Your OML WebPhone Client User username.
     'client_password': 'clientpassword',    // Your OML WebPhone Client User password.
}
```
2. Then you should install Flask and run it locally.
```sh
    $ pip install Flask
    $ pip install pyopenssl
    $ cd demo/
    $ FLASK_APP=views.py FLASK_DEBUG=1 flask run --cert=adhoc
```
Then you can navigate to:
    https://localhost:5000/


## <a name="wordpress-plugin"></a> WordPress plugin

We created a very simple WordPress plugin so you can insert the webphone client as a Widget in any part of your webpage. The plugin hasn't been published yet in wordpress repository so

**Installation:**

Like another wordpress plugin:

1. In your wordpress admin page go to Plugin -> Add new, and upload the ZIP file provided in *release/* folder
3. Click in "Install" button and wait is installed
4. Once installed Activate the plugin in the plugin's list

**Configuration:**

1. You will see an Admin page in Settings -> OMniLeads WebphoneClient where you need to configure your webphone client with the credentials of the webphoneclient user you created in your OMniLeads instance. **If we are hosting OML for you ask for the User's credentials**. Next type the URL to connect to the API.

<p align="center"><img src="doc_images/wp_admin_page.png" /></p>

2. Once you configure this settings go to the Widget menu where you will see the "OML WebphoneClient Widget" available. Here you will configure more settings:

<p align="center"><img src="doc_images/wp_widget_page.png" /></p>

  * **Title:** a title for your webphone
  * **Kamailio Host:** the LAN IP of your OMniLeads Instance **If we are hosting OML for you ask for this parameter**
  * **Styling Options:** the name of ID's and classes for your custom design (take a look at [Changing design of webphone](webphone-client-releases#changing-design-of-webphone) section)
  * **Call Buttons:** you can insert a maximum of four call buttons here you set the phone number to call and the nanme of the button.


## Changing design of WebPhone

The DOM for this application is this:

<p align="center"><img src="doc_images/webphone_client_dom.png" /></p>

With this DOM you can configure the ID's and classes you wish, and use them in your .css file to change the default design of the phone.
